const {ipcRenderer} = require("electron")
var fs = require("fs")
var path = require("path")
var util = require("util")
fs.readFilePromise = util.promisify(fs.readFile)
fs.writeFilePromise = util.promisify(fs.writeFile)
fs.mkdirPromise = util.promisify(fs.mkdir)

let storageDirectory = path.join(require("electron").remote.app.getPath("userData"), "playlists")

function clickPlaylist(event) {
	let playlistItem = this
	let playlist = JSON.parse(playlistItem.getAttribute("data-editc-playlist"))
	let title = playlist.title
	
	// debugger
	
	dismissPlaylistBrowser()
	
	// MARK: PLAYLIST LOAD
	// setAvailableTracks(tracks)
	setTracksInContainer(document.getElementById("playlist-panel-exploration"), playlist.exploration)
	setTracksInContainer(document.getElementById("playlist-panel-supercruise"), playlist.supercruise)
	setTracksInContainer(document.getElementById("playlist-panel-combat"), playlist.combat)
	setTracksInContainer(document.getElementById("playlist-panel-thargoids"), playlist.thargoids)
	setTracksInContainer(document.getElementById("playlist-panel-menu"), playlist.menu)
	setTracksInContainer(document.getElementById("playlist-panel-docking"), playlist.docking)
	
	// start a song
	playCategory(currentCategory)
	
	saveRestorePlaylist(playlist)
}

async function setupPlaylists() {
	let playlistBrowser = document.getElementById("playlist-browser")
	
	/* let dummyPlaylists = [
		{
			"name": "Playlist A",
			"exploration": [
				{
					"title": "ABC",
					"url": null,
				},
				{
					"title": "DEF",
					"url": null,
				}
			],
			"supercruise": [
				{
					"title": "GHI",
					"url": null,
				},
				{
					"title": "JKL",
					"url": null,
				}
			],
			"combat": [
				{
					"title": "MNO",
					"url": null,
				}
			],
			"thargoids": [
				{
					"title": "PQR",
					"url": null,
				}
			]
		}
	] */
	
	// let playlists = dummyPlaylists
	// let savedPlaylists = loadPlaylists()
	// playlists = playlists.concat(loadPlaylists())
	let playlists = await loadPlaylists()
	
	// debugger
	
	// remove everything
	while (playlistBrowser.hasChildNodes()) {
		playlistBrowser.removeChild(playlistBrowser.firstChild)
	}
	
	let newTable = document.createElement("table")
	for (var i = 0; i < playlists.length; i++) {
		let playlist = playlists[i]
		let row = document.createElement("tr")
		let col0 = document.createElement("td")
		// let col1 = document.createElement("td")
		
		col0.className = "playlist-table-name"
		
		let p = document.createElement("p")
		p.textContent = playlist.name
		col0.appendChild(p)
		
		row.addEventListener("click", clickPlaylist)
		
		// row.setAttribute("data-editc-playlist-title", playlist.name)
		// row.setAttribute("data-editc-playlist-tracklist", JSON.stringify(playlist.tracks))
		row.setAttribute("data-editc-playlist", JSON.stringify(playlist))
		
		row.appendChild(col0)
		// row.appendChild(col1)
		
		// add slide-out buttons container
		let buttonsContainer = document.createElement("div")
		buttonsContainer.className = "playlist-buttons-slideout"
		
		// add delete button
		let deletePreButton = document.createElement("button")
		let deleteImg = document.createElement("img")
		deleteImg.src = "icons/DeleteButton.svg"
		deleteImg.draggable = false
		deletePreButton.appendChild(deleteImg)
		
		deletePreButton.className = "delete-playlist-button"
		
		deletePreButton.addEventListener("click", function(event) {
			event.preventDefault()
			event.stopPropagation()
			// unhide actual delete button
			let row = this.parentNode.parentNode
			let button = row.querySelector(".delete-playlist-button-full")
			button.style.visibility = "visible"
			button.style.right = 0
			button.style.width = "auto"
			button.style.padding = "0px 8px 0px 8px"
		})
		
		let deleteButton = document.createElement("button")
		deleteButton.textContent = "Delete"
		deleteButton.className = "delete-playlist-button-full"
		
		deleteButton.addEventListener("click", function(event) {
			event.preventDefault()
			event.stopPropagation()
			let row = this.parentNode.parentNode
			let playlist = JSON.parse(row.getAttribute("data-editc-playlist"))
			deletePlaylistWithName(playlist.name)
			// reload list
			setupPlaylists()
		})
		
		let exportButton = document.createElement("button")
		let exportImg = document.createElement("img")
		exportImg.src = "icons/export.svg"
		exportImg.draggable = false
		exportButton.appendChild(exportImg)
		
		exportButton.className = "playlist-action-button"
		
		exportButton.addEventListener("click", function(event) {
			event.preventDefault()
			event.stopPropagation()
			let row = objectOrParentOfType(this, "tr")
			let playlist = row.getAttribute("data-editc-playlist")
			// format the JSON data
			playlistObject = JSON.parse(playlist)
			prettyPlaylist = JSON.stringify(playlistObject, null, "\t")
			exportPlaylistWithJSONData(prettyPlaylist)
		})
		
		buttonsContainer.appendChild(exportButton)
		buttonsContainer.appendChild(deletePreButton)
		
		col0.appendChild(buttonsContainer)
		col0.appendChild(deleteButton)
		
		newTable.appendChild(row)
	}
	
	playlistBrowser.appendChild(newTable)
	
	playlistBrowser.addEventListener("mouseleave", function(event) {
		// hide all delete buttons on mouse leave
		// console.log("Received hide delete buttons event: " + this)
		// console.log(this.nodeName.toLowerCase())
		// console.log("Hiding delete buttons")
		// event.preventDefault()
		event.stopPropagation()
		let buttons = this.querySelectorAll(".delete-playlist-button-full")
		for (i = 0; i < buttons.length; i++) {
			let button = buttons[i]
			button.style = ""
		}
	}, false) // , false: invert responder chain -- top down!
}

// local storage

function sortPlaylists(playlists) {
	playlists.sort(function(a, b) {
		if (a.name < b.name) {
			return -1
		} else if (a.name > b.name) {
			return 1
		}
		// same name
		aStr = JSON.stringify(a)
		bStr = JSON.stringify(b)
		if (aStr < bStr) {
			return -1
		} else if (aStr > bStr) {
			return 1
		}
		// same contents, too!
		return 0
	})
	return playlists
}

async function loadPlaylists() {
	console.log("Loading playlists")
	let storageDirContents = fs.readdirSync(storageDirectory)
	let jsonFiles = storageDirContents.filter((f) => f.endsWith(".json"))
	console.log("All playlists:", jsonFiles)
	// read all files
	let promises = jsonFiles.map((f) => {
		// for every playlist, read its file...
		let fullPath = path.join(storageDirectory, f)
		return fs.readFilePromise(fullPath, {"encoding": "utf8"})
		.then((contents) => {
			// and parse it, later
			return JSON.parse(contents)
		})
	})
	// wait for all the file reading and stuff to finish
	let playlists = await Promise.allSettled(promises).then((results) => results.map((result) => {
		// for this file, what happened?
		if (result.status == "fulfilled") {
			return result.value
		} else {
			console.error(result.reason)
			// throw result.error
			return null
		}
	}))
	// filter out the ones that didn't work
	playlists = playlists.filter((p) => p != null)
	console.log("Loaded playlists:", playlists)
	return playlists
}
function savePlaylist(playlist) {
	let title = playlist.name
	let filename = title + ".json"
	let fullPath = path.join(storageDirectory, filename)
	
	console.log("\tSaving playlist: " + title)
	console.log("\tPath of file to save: " + fullPath)
	let data = JSON.stringify(playlist, null, "\t")
	let saveOptions = {
		flag: "w"
	}
	try {
		fs.writeFileSync(fullPath, data, saveOptions)
		console.log(title + " saved successfully")
	} catch (error) {
		console.error("Couldn't save playlist: " + error)
		alert("Couldn't save playlist: " + error)
	}
}
function deletePlaylistWithName(name) {
	let filename = name + ".json"
	console.log("Deleting " + filename)
	let fullPath = path.join(storageDirectory, filename)
	try {
		fs.unlinkSync(fullPath)
		console.log(name + " deleted successfully")
	} catch (error) {
		console.error("Couldn't delete playlist: " + error)
		alert("Couldn't delete playlist: " + error)
	}
}

function exportPlaylistWithJSONData(playlistData) {
	console.log("Sending "+playlistData+" to main process for saving")
	ipcRenderer.send("export-playlist", playlistData)
}
function startImportPlaylist() {
	ipcRenderer.send("start-import-playlist")
}
ipcRenderer.on("import-playlist-paths", function(event, paths) {
	console.log("Renderer received paths: "+JSON.stringify(paths))
	for (i = 0; i < paths.length; i++) {
		let path = paths[i]
		console.log("Opening "+path)
		let playlistJSON = fs.readFileSync(path, "utf8")
		let playlist = JSON.parse(playlistJSON)
		savePlaylist(playlist)
	}
	setupPlaylists()
})

// mini save dialog

// see https://stackoverflow.com/a/896774/8365799
function focusMiniSaveNameField(iteration = 0) {
	// console.log("Iteration " + iteration)
	let textField = document.getElementById("new-playlist-name-field")
	textField.focus()
	if (iteration > 10 || document.activeElement == textField) { // stop if focus succeeded
		return
	} else {
		setTimeout("focusMiniSaveNameField(" + (iteration+1) + ")", 100)
	}
}

function showMiniSaveDialog() {
	let dialog = document.getElementById("new-playlist-mini-dialog")
	dialog.style.visibility = "visible"
	dialog.style.opacity = 1.0
	dialog.style.width = "512px"
	dialog.style.left = "0px" // slide in
	
	focusMiniSaveNameField()
}
function dismissMiniSaveDialog() {
	let dialog = document.getElementById("new-playlist-mini-dialog")
	dialog.style = ""
	
	// clear whatever's in the text field
	let textField = document.getElementById("new-playlist-name-field")
	textField.value = ""
}

// set up button actions

document.getElementById("new-playlist-button").addEventListener("click", showMiniSaveDialog)
// Esc in save dialog cancels
document.getElementById("new-playlist-mini-dialog").addEventListener("keyup", function() {
	if (event.key == "Escape") {
		event.stopPropagation() // stop propagation up the responder chain
		dismissMiniSaveDialog()
	}
})

document.getElementById("playlist-import-button").addEventListener("click", startImportPlaylist)

function saveNewPlaylist() {
	let playlistName = document.getElementById("new-playlist-name-field").value
	
	let playlist = {}
	playlist.name = playlistName
	
	function populate(category) {
		playlist[category] = []
		var table = document.getElementById("playlist-panel-" + category).querySelector("table")
		if (table != null) {
			console.log(category + " table rows: " + JSON.stringify(table.rows))
			for (var i = 0; i < table.rows.length; i++) {
				let row = table.rows[i]
				let data = row.getAttribute("data-editc-track-info")
				console.log(category + " track data: " + data)
				playlist[category].push(JSON.parse(data))
				debugger
			}
		}
	}
	
	// MARK: PLAYLIST SAVE
	populate("exploration")
	populate("supercruise")
	populate("combat")
	populate("thargoids")
	populate("menu")
	populate("docking")
	
	console.log("FINISHED PLAYLIST: " + JSON.stringify(playlist))
	
	// append to playlists
	savePlaylist(playlist)
	
	// reload list
	setupPlaylists()
	
	// dismiss mini dialog
	dismissMiniSaveDialog()
}
document.getElementById("new-playlist-save-button").addEventListener("click", saveNewPlaylist)
document.getElementById("new-playlist-name-field").addEventListener("keypress", function() {
	if (event.key == "Enter") {
		event.stopPropagation()
		event.preventDefault()
		saveNewPlaylist()
	}
})

async function migrateLocalStorageToConfig() {
	console.log("Storage directory for migration: " + storageDirectory)
	
	// make sure the storage directory exists
	if (!fs.existsSync(storageDirectory)) {
		try {
			await fs.mkdirPromise(storageDirectory, {"recursive": true})
		} catch (error) {
			console.error("Couldn't create playlist saves directory: " + error)
			alert("Couldn't create playlist saves directory: " + error)
			return
		}
	}
	
	let playlists = JSON.parse(localStorage.getItem("playlists"))
	if (playlists) {
		console.log("Migrating local storage to config")
		// console.log("All playlists: " + playlists)
		
		let succeededPlaylists = []
		let failedPlaylists    = []
		
		for (playlist of playlists) {
			let title = playlist.name
			let filename = title + ".json"
			let fullPath = path.join(storageDirectory, filename)
			if (fs.existsSync(fullPath)) {
				console.log(title + " already migrated")
			} else {
				console.log("\tSaving playlist: " + title)
				console.log("\tPath of file to save: " + fullPath)
				let data = JSON.stringify(playlist)
				let saveOptions = {
					flag: "wx" // don't overwrite existing files
				}
				try {
					await fs.writeFilePromise(fullPath, data, saveOptions)
					console.log(title + " migrated successfully")
					succeededPlaylists.push(title)
				} catch (error) {
					console.error("Couldn't migrate playlist " + title + ": " + error)
					failedPlaylists.push({"title": title, "error": error})
				}
			}
		}
		if (failedPlaylists.length > 0) {
			let allErrorStrings = failedPlaylists.map((e) => e.error).join("\n")
			alert("Couldn't migrate " + failedPlaylists.length + " playlists:\n\n" + allErrorStrings)
		}
		if (succeededPlaylists.length > 0) {
			alert(succeededPlaylists.length + " playlists migrated successfully")
		}
	}
}

migrateLocalStorageToConfig()
