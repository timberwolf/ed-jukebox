let { dialog } = require("electron").remote
console.log("Dialog module:", dialog)

function setupSelectJournalsButton() {
	let selectJournalsButton = document.getElementById("select-journals-button")
	selectJournalsButton.addEventListener("click", (event) => {
		dialog.showOpenDialog({
			title: "Select Journal Path",
			properties: [
				"openDirectory",
				"showHiddenFiles"
			]
		}).then((openPanelReturn) => {
			let journalFolder = openPanelReturn.filePaths[0]
			console.log("Saving journal folder:", journalFolder)
			localStorage.setItem("JournalPath", journalFolder)
		})
	})
}

setupSelectJournalsButton()
